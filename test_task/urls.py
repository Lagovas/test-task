from django.conf.urls import patterns, include, url

from django.contrib import admin

from test_task.apps.feed.views import HomeView, LoadExcelView
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^feed/(?P<slug>[a-zA-Z0-9_-]+?)/$', HomeView.as_view(), name='feed'),
    url(r'^excel/(?P<slug>[a-zA-Z0-9_-]+?)/$', LoadExcelView.as_view(), name='load-excel'),
    url(r'^admin/', include(admin.site.urls)),
)
