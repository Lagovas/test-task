import re
import requests
import logging
from django.conf import settings

logger = logging.getLogger(__name__)
__feed_data_pattern = re.compile(r'href="(?P<url>.+?\.xml)".+?>(?P<name>.+?)</a>')


def load_feeds():
    response = requests.get(settings.FEED_URL)
    data_iterator = __feed_data_pattern.finditer(response.text)
    if not data_iterator:
        logger.fatal(u'Feeds were not found. Maybe a regular expression did not work.')
        return
    feeds = []
    for data in data_iterator:
        feeds.append(data.group('name', 'url'))

    return feeds