from django.db.models.signals import post_syncdb
import test_task.apps.feed.models
from test_task.apps.feed.cron import activate_cron
from test_task.apps.feed.models import Feed, FeedItem

post_syncdb.connect(activate_cron, test_task.apps.feed.models)
post_syncdb.connect(Feed.load_feeds, test_task.apps.feed.models)
post_syncdb.connect(FeedItem.update_feed_items, test_task.apps.feed.models)
