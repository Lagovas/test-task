from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext as _
from transliterate import slugify
import autoslug
import feedparser
from dateutil import parser
from test_task.apps.feed.utils import load_feeds


class Feed(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('Feed name'))
    url = models.URLField(verbose_name=_('Url'))
    etag = models.CharField(max_length=30, blank=True, null=True)
    slug = autoslug.AutoSlugField(
        populate_from='name', slugify=lambda value: slugify(value, 'ru'))

    def get_absolute_url(self):
        return reverse('feed', kwargs={'slug': self.slug})

    @classmethod
    def load_feeds(cls, *args, **kwargs):
        if cls.objects.all().count() != 0:
            return
        feeds = load_feeds()
        for name, url in feeds:
            cls.objects.create(name=name, url=url)

    def __unicode__(self):
        return self.name


class FeedItem(models.Model):
    title = models.CharField(max_length=255, verbose_name=_('Title'))
    description = models.TextField(verbose_name=_('Description'))
    link = models.URLField()
    datetime = models.DateTimeField()
    feed = models.ForeignKey(Feed)


    @classmethod
    def update_feed_items(cls, *args, **kwargs):
        """download and parse new entries for all feeds

        korrespondent.net dont care about modified and etag headers, so new
        entries will checked using existing entries in db
        """
        feeds = Feed.objects.all()
        for feed in feeds:
            print u'Download entires for feed: {feed}'.format(feed=feed.name)
            data = feedparser.parse(feed.url)
            data_count = len(data.entries)
            feeditems = (FeedItem.objects.filter(feed=feed)
                         .order_by('-datetime')
                         .values_list('title', flat=True)[:data_count])
            for item in data.entries:
                if item.title in feeditems:
                    continue
                feed_item = FeedItem()
                feed_item.title = item.title
                feed_item.description = item.description
                feed_item.link = item.link
                feed_item.datetime = parser.parse(item.published)
                feed_item.feed = feed
                feed_item.save()


    def __unicode__(self):
        return self.title