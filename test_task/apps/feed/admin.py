from django.contrib import admin
from test_task.apps.feed.models import FeedItem, Feed
admin.site.register(Feed)
admin.site.register(FeedItem)