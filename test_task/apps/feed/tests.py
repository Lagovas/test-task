import random
from StringIO import StringIO
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.utils.html import escape
import xlsxwriter
from test_task.apps.feed.models import Feed


class ExcelTest(TestCase):

    def test_downloading(self):
        feed = Feed.objects.all().first()
        response = self.client.get(reverse('load-excel', kwargs={'slug':feed.slug}))
        feed_items = feed.feeditem_set.all()
        output = StringIO()
        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet()
        for i in xrange(len(feed_items)):
            worksheet.write(i, 0, feed_items[i].description)
        workbook.close()
        value = output.getvalue()
        output.close()
        self.assertEqual(value, response.content)
        self.assertEqual(len(value), len(response.content))

    def test_download_url(self):
        feed = Feed.objects.all().first()
        response = self.client.get(reverse('feed', kwargs={'slug': feed.slug}))
        self.assertIn(reverse('load-excel', kwargs={'slug': feed.slug}), response.content)


class HomeViewTest(TestCase):

    def test_index_page(self):
        feed = Feed.objects.all().first()
        feed_items = feed.feeditem_set.all()
        response = self.client.get(reverse('home'))
        for item in feed_items:
            self.assertIn(item.description, response.content.decode('utf-8'))
            self.assertIn(escape(item.title), response.content.decode('utf-8'))

    def test_feed_page(self):
        feed = Feed.objects.values_list('id', flat=True)
        feed = feed[1:]
        feed_id = random.choice(feed)
        feed = Feed.objects.get(pk=feed_id)
        feed_items = feed.feeditem_set.all()
        response = self.client.get(reverse('feed', kwargs={'slug':feed.slug}))
        for item in feed_items:
            self.assertIn(item.description, response.content.decode('utf-8'))
            self.assertIn(escape(item.title), response.content.decode('utf-8'))