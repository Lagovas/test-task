from StringIO import StringIO
from django.shortcuts import render_to_response
from django.views.generic import View
from django.http import Http404, HttpResponse
import xlsxwriter
from test_task.apps.feed.models import Feed


class HomeView(View):
    template_name = 'home.html'

    def get(self, *args, **kwargs):
        feeds = Feed.objects.all()
        if 'slug' in self.kwargs:
            current_feed = (Feed.objects
                            .filter(slug=self.kwargs['slug'])
                            .prefetch_related('feeditem_set').first()
            )
            if current_feed:
                feeditems = current_feed.feeditem_set.all()
            else:
                raise Http404
        else:
            current_feed = feeds.first()
            feeditems = current_feed.feeditem_set.all()
            print 'feeditems'
        return render_to_response(
            self.template_name, {
                'feeds': feeds,
                'current_feed': current_feed,
                'feeditems': feeditems})

class LoadExcelView(View):

    def get(self, *args, **kwargs):
        if 'slug' in self.kwargs:
            current_feed = (Feed.objects
                            .filter(slug=self.kwargs['slug'])
                            .prefetch_related('feeditem_set').first()
            )

            feeditems = current_feed.feeditem_set.all()
            output = StringIO()
            response = HttpResponse(content_type=u'application/vnd.ms-excel')
            response['Content-Disposition'] = (u'attachment; filename="feed.xslx"')
            workbook = xlsxwriter.Workbook(output)
            worksheet = workbook.add_worksheet()
            for i in xrange(len(feeditems)):
                worksheet.write(i, 0, feeditems[i].description)
            workbook.close()
            value = output.getvalue()
            output.close()
            response.write(value)
            return response
        else:
            raise Http404