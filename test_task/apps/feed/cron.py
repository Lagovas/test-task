from django.core.management import call_command
import kronos
import test_task.apps.feed.models
from test_task.apps.feed.models import FeedItem

@kronos.register('15 * * * *')
def update_feed_items():
    FeedItem.update_feed_items()

def activate_cron(*args, **kwargs):
    call_command('installtasks')